layui.use(['table'], function(){
    var table = layui.table;

    //第一个实例
    var tableIns = table.render({
		elem: '#layTable'
        ,height: 315
        ,url: baseURL + 'sys/user/list2' //数据接口
        ,page: true //开启分页
		,id: 'layTable'
        ,cols: [[ //表头
            {field: 'userId', title: '用户ID',  sort: true, fixed: 'left'}
            ,{field: 'username', title: '用户名'}
            ,{field: 'email', title: '邮箱',  sort: true}
            ,{field: 'mobile', title: '手机号'}
            ,{field: 'status', title: '状态', templet: '#convertTpl' }
             //数据字段

        ]]
    });

});



/*
laytable的返回值格式一定是这样的
{
    "msg":"success",
    "code":0,
    "data":[
    {
        "userId":1,
        "username":"admin",
        "password":"308bac1af5bb107476b26030cd8f2780e96c4e432e010b22e54150dabef7f6f2",
        "salt":"YzcmCZNvbXocrsz9dm8e",
        "email":"root@renren.io",
        "mobile":"13612345678",
        "status":1,
        "roleIdList":null,
        "createUserId":1,
        "createTime":"2016-11-11 11:11:11"
    }
],
    "count":1
}*/
