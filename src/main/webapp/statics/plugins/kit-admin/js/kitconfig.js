layui.define(function(exports) {

    var kitconfig = {
        resourcePath: './statics/plugins/kit-admin/', //框架资源路径-相对路径和绝对路径, 需要把原来kit-admin中的src中内容引用进来
    };

    exports('kitconfig', kitconfig);
});